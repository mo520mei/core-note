# CORE原理
这个文档主要说明了一下必要的 CORE（Common Open Research Emulator） 的安装以及简单的实现原理

## CORE的主要进程

### core-daemon
这个是 CORE 的[守护进程][1]，它主要负责管理仿真会话、生成节点（[linux namespace][2]）、链路（[linux bridge and virtual ethernet peers][3]）、响应 [GUI](#core-pygui) 的控制、提供grpc和tlv通信api。

### core-pygui
这是 CORE 新版的基于 python 的 GUI ，老版的 GUI 基于 tlv 语言，难以维护和改进，目前基本已经停止更新，所以不再赘述，也不推荐使用老版GUI（一下如非特别声明，所提到的GUI均为新版的core-pygui）。该 gui 通过 gRPC API 与 [core-daemon](#core-daemon) 进行通信
在 GUI 中，你可以：

* 通过拖拽绘制网络拓扑
* 保存/读取自己绘制的网络拓扑
* 启动仿真中节点的终端

## CORE的安装

### 单服务器安装

1. 首先，从 github 上下载 CORE 的源代码（ github 打不开的情况下，后面两种方法比较好）。如果不是用 git 获取的源代码，还需要解压。
    * 访问[CORE的github](https://github.com/coreemu/core)页面下载源代码或者某个release版本的代码。
    * `git clone https://github.com/coreeum/core.git`
    * `git clone git://github.com/coreemu/core.git`
    * `wget https://github.com/coreemu/core/releases/tag/release-7.5.1`
2. 进入 CORE 的代码文件夹。
   ```bash
   # 具体路径视实际情况而定。
   cd core
   ```
3. 执行安装脚本
   ```bash
   ./install.sh
   ```
4. 安装 EMANE（可选）
   ```bash
   inv install-emane
   ```

### 分布式安装
为了方便起见，最好几台服务器的IP地址固定。

**注意事项**：
* 主服务器安装`openssh-client`
* 从服务器安装`openssh-server`、`xterm`。
* 所有服务器上有一个相同的普通用户名，这个用户名也是我们在主服务器上启动CORE的时候所登录的用户名。


1. 依据[单服务器安装](#单服务器安装)在主服务器上安装 CORE 。
2. 在主服务器上编辑 `/etc/core/core.conf` 文件（注意需要 sudo 或者 root 账户）。
   ```bash
   # *.*.*.* 代表了主服务器的IP地址，这个IP地址用于从服务器向主服务器回传消息。
   distributed_address = *.*.*.*

   # EMANE 需要有 controlnet 保证正常启动，所以需要如下配置：
   # 可以根据自己的仿真规模确定 controlnet 的个数。这个地址不是某个服务器的地址，纯粹是仿真中用的的地址，自己随意指定一个不会冲突的地址段就行了。
   controlnet = core1:172.16.1.0/24 core2:172.16.2.0/24 core3:172.16.3.0/24 core4:172.16.4.0/24 core5:172.16.5.0/24
   emane_event_generate = True
   ```
3. 在所有的从服务器上安装 [core_distributed][4]
   ```bash
   # 网页下载core_distributed的安装包或者使用wget下载
   wget https://github.com/coreemu/core/releases/download/release-7.5.1/core_distributed_7.5.1_amd64.deb
   # 安装core_distributed
   sudo dpkg -i core_distributed_version_amd64.deb
   #如果碰到依赖问题安装失败，则执行下述命令。
   sudo apt --fix-broken install
   # 上面这个修复依赖问题的命令中应该会自动安装xterm。
   ```
4. 配置 SSH
如果你给节点设置的终端是 xterm，则需要在所有服务器上安装xterm。
   ```bash
   sudo apt install xterm
   ```
    **以下步骤为配置 root 账户的无密码 SSH 登录**

    1). 在主服务器上安装 SSH 客户端并生成密钥

    ```bash
    sudo apt install openssh-client # 安装 SSH 客户端
    ssh-keygen -o -t rsa -b 4096 -f ~/.ssh/core # 生成 SSH 密钥，当系统提示输入密码时，只需按 Enter 键即可生成没有密码的密钥
    ```
    2). 在从服务器上安装 SSH 服务端并修改配置文件

    ```bash
    # 安装 SSH 服务端并打开配置文件
    sudo apt install openssh-server
    sudo vi/vim/gedit /etc/ssh/sshd_config

    # 在打开的文件中修改以下内容
    PasswordAuthentication yes
    PermitRootLogin yes

    # 重启服务器
    sudo service ssh restart

    # 设置 root 密码
    sudo passwd
    ```
    3). 在主服务器上将密钥传输到从服务器

    ```bash
    ssh-copy-id -i ~/.ssh/core root@*.*.*.* # 此处为从服务器 IP，当询问是输入从服务器 root 密码
    ssh -i ~/.ssh/core root@*.*.*.* #测试 SSH 配置
    ```
    4). 在从服务器上修改 SSH 配置文件

    ```bash
    sudo passwd -l root
    sudo vim /etc/ssh/sshd_config

    # 修改以下内容
    # 这种修改主要是为了安全问题，自己测试的话修不修改无所谓
    PasswordAuthentication = no
    PermitRootLogin without - password

    # 重启服务
    sudo service ssh restart
    ```
    5). 在主服务器上测试并修改配置

    ```bash
    ssh -i  ~/. ssh / core root@*.*.*.* # 测试无密码登录
    sudo vim /etc/fabric.yaml
    # 写入以下内容：
    connect_kwargs: {"key_filename": "/home/user/.ssh/core"}# user 替换为具体用户名。
    ```
    **此外还需要配置普通用户的无密码SSH**

    1). 从服务上修改 SSH 配置文件
    ```bash
    sudo vim /etc/ssh/sshd_config

    # 修改以下内容
    PasswordAuthentication = yes

    # 重启服务
    sudo service ssh restart
    ```
    2). 在主服务器上传输密钥文件
    ```bash
    ssh-copy-id -i ~/.ssh/core user@*.*.*.* # user是用户名，这里最好所有服务器上有一个相同的普通用户名。此处为从服务器 IP，当询问是输入从服务器 user 密码
    ssh -i ~/.ssh/core root@*.*.*.* #测试 SSH 配置
    ```


5. 在 core-pygui 中：可以在 Session->Servers中添加分布式服务器

### CORE 的安装原理
这里简单讲一下 CORE 在安装过程中所涉及的一些东西。

首先是`install.sh`文件，这个 bash 文件中首先检查了传入的参数，然后安装了 python3、python3-venv、pip、pipx、invoke 等软件包。然后执行命令 `inv install` 来安装 CORE。

其次是 `tasks.py` 这个文件是 invoke 会调用的任务文件，其中定义了许多任务（以方法的形式）。CORE 的安装就是调用了其中 `install` 这个任务。

所以想要修改 CORE 的安装过程的话就修改这两个文件。

## CORE仿真原理分析

### 启动 core-daemon
通过 `sudo core-daemon` 或者 CORE 文档中其他方式启动 core-daemon 后，可以在系统中看到一个 `core-daemon` 守护进程（`ps -ef | grep core`），该进程会一直通过 gRPC 通信接口监听消息。以便随时接收 core-pygui 的消息并执行相应动作（开始仿真、移动节点、启动节点终端等）并将仿真中的一些结果信息反馈给 GUI。

### 启动 core-pygui
通过 `core-pygui` 命令启动 core-pygui，启动后该进程会生成一个GUI界面供用户使用。在 core-pygui 中的操作都将先生成一系列配置信息存储在内存中，当然你也可以吧这些信息保存到硬盘中的一个.xml文件中。直到点击 **开始仿真按钮** GUI 会把这些信息通过 gRPC 接口发送给 core-daemon 进程。仿真开始后，可以在 GUI 中移动节点，打开节点终端等，这些都会实时与 core-daemon 通信，core-daemon 执行动作后返回结果。

### CORE 中的节点类型
我们可以看到 CORE 中的节点主要分为两种类型：CoreNode 和 CoreNetwork。

相关代码主要在 `/core/daemon/core/node` 中。其中，先定义了 NodeBase 类，然后由这个类派生了其他节点类。

#### CoreNode
这类节点是基于 linxu namespace 实现的节点，在 GUI 中对应 router、PC 等三层节点。

它们在仿真开始后可以双击节点打开该节点的终端，在终端中执行的命令，CORE 会自动给它加上前缀保证命令在这个节点对应的命名空间中执行。此外，对于每个corenode，CORE还会为其在硬盘中创建一个临时文件夹作为这个命名空间的根目录用来存储节点相关文件，比如我们的服务脚本。

#### CoreNetwork
这个节点类由 linux bridge 实现，它派生出了其他的 2 层节点，比如：switch、ptpnet等（具体可以参考 `/core/node/network.py`）。这些节点的具体实例我在后面统一称为 network node。

### CORE 中网络拓扑的实现
搭建一个CORE中的网络，我们需要用的有 corenode 和 networknode 以及有线链路(PtPnet)（如果需要的话）。
这两类节点前者由 linux namespace 实现，后者（包括有线链路）由 linux bridge 实现。

#### corenode 到 corenode 的有线链路实现原理
* 实例化一个有线链路（ptpnet）节点，并启动一个 linux bridge 与之关联。我们可以在主机的终端中执行 `ip link` 查看，其中名字为“b.*.*”的即为我们仿真创建的网桥。
* 创建一个 veth-pair 并将其一段放到 corenode1 中，一端绑定到 ptpnet 节点的网桥上。
* 同理，通过 veth-pair 连接 corenode2 和 ptpnet 节点。

同时，我们可以用`ip link`命令查看CORE所创建的veth-pair中挂载到网桥上的veth。其中每个veth有参数是master，后面跟的网桥名，就是他们挂载的网桥。

#### corenode 连接到同一个 networknode 的实现原理
因为 networknode 本身就是一个网桥，所以猜测大概原理就是通过 veth-pair 连接 corenode 和 networknode。具体见 /core/emulator/session.py 中的 add_link() 方法，这个我在代码解读文档中也有说明。

### CORE中服务实现原理
CORE中的服务，不管多复杂，他们的实现原理都一样（只有corenode可以配置服务）。
他们都继承了CoreService这个类。并重写了自己的部分参数和方法。

每个服务有几个比较重要的参数：
* group：指定这个服务属于那一组
* name：服务名字
* executable：这个参数中包含了我们在写服务的配置文件的时候所要用的可执行文件（即我们的主机中有没有安装对应的软件，比如OVS，EMANE）。
* dependency：顾名思义，依赖项，这里的依赖项指的不是依赖某个软件包，而是依赖某个服务，即这个服务必须在其依赖的服务启动之后启动（启动顺序的问题，CORE会自行解决，不需要担心，配置好依赖项就行了）
* config: 这个是服务配置文件的文件名列表
* startup: 这个是启动服务的命令
* shutdown: 停止服务的命令
* validate: 验证服务的命令

每个服务最重要的方法就是`generate_config()`方法，这个方法会根据文件名来返回对应的配置文件内容。所生成的文件一般会被startup或者shutdown所使用。

可以看到每个服务都有三个参数是启动、关闭相关的命令，这些命令的执行统统是在对应节点的命名空间中执行的，所以我们在startup的命令或者该命令使用的配置文件中配置OVS的启动，那么这个系欸但中的OVS进程就只能看到这个节点中的eth接口。

### CORE中的仿真启动过程
这里只是简述一下流程，具体实现细节，代码解读中有。
1. core-pygui通过grpc通信将开始仿真的指令和仿真所需要的参数发给core-daemon。
2. core-daemon读取参数
3. 创建节点（创建一个命名空间并配置根目录、名字、账户等，或者创建网桥并配置）
4. 创建链路（根据情况创建ptpnet、或者简单的veth-pair，并将对应的veth放到命名空间中或挂载到网桥中）
5. 启动节点的服务（在节点的命名空间中执行每个服务中startup参数所配置的命令）


## OVS运行原理
安装OVS后，我们会通过一些列命令启动我们的OVS，那么在这个过程中，都涉及了哪些东西呢？

* conf.db文件：这个文件是存储我们的交换机信息的数据库文件。
* ovsdb-server进程：这个进程是OVS的数据库进程，OVS通过这个进程读取、修改数据库文件。
* ovs-vswitchd进程：这个是OVS交换机的守护进程，类似core-daemon。
* ovs-vsctl：这个命令主要用来对OVS数据库进程操作，并管理网桥的“硬件”（网桥，port等）。
* ovs-ofctl：这个命令主要用来对具体网桥内部的“软件”（流表等）进行管理。

需要说明的是，conf.db文件、ovsdb-server和ovs-vswitchd进程必须一一对应。不同的ovs-vswitchd进程必须连接不同的ovsdb-server，不同的ovsdb-server必须打开不同的conf.db文件。

那么，这种一一对应该如何实现呢？通过指定不同的文件名来实现，我们可以把一个空白的数据库文件复制若干份，分别命名。然后再启动ovsdb-server进城的时候指定这个进程用的数据库文件的文件名，并同时指定自己的pid文件名、log文件名和socket文件名。最后，在启动ovs-vswitchd的时候指定连接的socket文件名以连接到对应的数据库，同时指定自己的pid文件名、log文件名。最最后，在使用ovs-vsctl命令的时候必须指定socket文件，但是ovs-ofctl命令不需要。

需要注意的是，上面所涉及的文件中我们只需要在命令中写文件名就可以了，OVS默认会把文件放在硬盘的固定位置，分别是：

* `/usr/local/etc/openvswitch/`存放数据库文件
* `/usr/local/var/run/openvswitch/`存放pid文件和socket文件
* `/usr/local/var/log/openvswitch/`存放log文件

如果这些路径不存在，请自行使用`sudo mkdir`命令自行创建。


## linux namespace
这里主要讲一下veth接口和network namespace的东西。
为了隔离不同节点的网络，我们需要为每个corenode创建独立的linux networknamespace。
这里的隔离就导致了在某个特定的命名空间（包括我们主机的命名空间）中，我们看不到在其他命名空间中的veth。

那么，对于进程来说，运行在某个命名空间中的进程也只能看到自己的命名空间中的网络信息，比如veth。
所以，在某个特定命名空间中运行的进程自然也看不到在其他命名空间中的veth。这也就是为什么我们要在每个corenode内部都启动一套ovs进程（ovsdb-server和ovs-vswitchd）。

需要注意的是，CORE中使用namespace这个东西只隔离了进程和网络，并指定了每个命名空间的根目录，但是实际上，硬盘中的所有文件是主机和所有命名空间共享的。

## 关于我在开发OVS服务中遇到的问题

### 1. ovs网桥检测不到节点内的eth接口。
这是因为最开始我实在python代码的StartSession方法中启动的ovs的两个进程，也就是说这俩进程运行在主机的命名空间中，所以自然也就无法检测到节点内部的eth接口，因为这些接口已经放到了节点的命名空间中。当我在节点中运行ovs进程时这个问题就解决了

### 2. 多个ovs交换机只有一个生效，即只有一个ovs进程组（ovsdb-server&ovs-vswitchd）以及为什么可以在不同的命名空间中查看其他命名空间中的网桥信息。
这是因为CORE中的节点基于Linux namespace，他们只隔离了网络栈和进程，但是文件是共享的。而ovs的所有信息均储存在主机的`/usr/local/etc/openvswitch/`下的数据库文件中。考虑到同一个数据库文件只能被一个ovsdb-serveer进程占用，所以当多个ovsdb-server进程想要使用同一个数据库文件时，后启动的ovsdb-server就会启动失败。解决方案就是使用不同的数据库文件，可以直接复制原数据库文件并重命名。同时，因为数据库文件都可以访问，所以就可以在某个命名空间中查看其他命名空间中的ovs网桥信息。

### 3. 为什么大多数paper和教程中只有单个子网（网段）内的SDN
因为简单，不需要配置复杂的功能，只需要配置一些转发规则就可以了。

### 4. 如何实现跨子网SDN
1. 考虑到我们用来做终端的节点一般是CORE中的PC节点，他的“DefaultRoute”服务会配置默认路由到自己子网段的`*.*.*.1`地址，所以我们在GUI的代码中的router_nodes和node_moudles中加入了ovswitch。保证ovs交换机不同端口连接的是不同网段，且给ovs交换机分配的IP地址为`*.*.*.1`。
2. 那么PC A要ping通交换机另一段连接的PC B，首先PC都必须知道自己的网关在哪里，即网关的MAC地址。由计算机网络的知识可以知道，PC是通过ARP过程来发现子网内其他接口的MAC地址并将IP地址和MAC地址对应关系放入ARP表中（通过`arp -n`或`ip neigh show`命令可以查看）。那么我们也就需要让ovs交换机实现回复PC的APR请求的功能。这样就可以让PC知道自己的数据链路层帧头中的目的MAC地址应该填什么而不是填一个广播地址。具体实现见代码（获取MAC地址可以用`ip link`）。
3. 此外，还可以配置ovs交换机的ICMP报文回复功能来实现PC可以ping通OVS交换机。
4. 在此基础之上，来配置ovs交换机真正的跨子网转发的功能了。显然，需要配置的流表应该是匹配子网段，然后进行转发。但是仅仅如此是有问题的，因为我们只是把PC A的数据包转发到的PC B的子网中，但是数据包的MAC层信息还没有修改，这使得目的MAC地址仍然是ovs交换机在PC A的子网中的哪个eth口的MAC地址，显然，PC B并不会回应这个ICMP报文。所以，我们在转发的同时，还需要根据目的IP地址来修改数据包的源目的MAC地址。如果觉得把这些都写到一个流表内太麻烦，可以考虑先匹配网段，然后把流丢给下一个流表来匹配IP地址修改MAC信息后转发。这样也比较有可拓展性。如果只是简单地测试该功能的话就随意了。

## 常用命令
<center>常用的命令参考表</center>

| 命令 | 常用参数 | 作用 |
|:-:|:-:|:-:|
| ps | -ef | 查看进程 |
| grep | abc | 抓取终端返回的信息中包含abc关键字的信息 |
| tcpdump | -vxn -i InterfaceName | 抓取某个eth接口收到的数据包 |
| ping | -c times ipaddr| 使用ICMP报文检查ipaddr的可达性 |
| ovs-vsctl | --db=unix:db.sock command | 这里的参数主要是为了指定数据库的socket文件，常用的附加指令有add-bridge、add-port等 |
| ovs-ofctl | command ovsbrname | 这个命令不需要指定socket文件，只需要指定网桥名就可以了，常用命令有add-flow、del-flows(匹配关键字)、dump-dlows、show |
| ip | command | linux非常强大的网络工具包，基本可以代替曾经的nettools工具包，命令可以有link查看eth接口，addr 查看ip配置，neigh(ARP表)、route(路由表)等 |

**注**：这里列出的一些常用的命令的简单用法，详细内容请 command --help 获取。



[1]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiYiv-H8f3yAhXP454KHdXeCj8QFnoECA4QAw&url=https%3A%2F%2Fblog.csdn.net%2Fweicao1990%2Farticle%2Fdetails%2F78639549&usg=AOvVaw1OlhnlfLTi5um4OWSgUVU4
[2]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjb-rTQ8f3yAhXHyp4KHU4nAD4QFnoECBAQAw&url=https%3A%2F%2Fwww.cnblogs.com%2Fsparkdev%2Fp%2F9365405.html&usg=AOvVaw3-T1hCZaL70JhKwk_kLjlJ
[3]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjc6Ned8v3yAhVRwZ4KHYW3CEAQFnoECAkQAQ&url=https%3A%2F%2Fblog.csdn.net%2FLL845876425%2Farticle%2Fdetails%2F82156405&usg=AOvVaw06gZB7YrRamOr27GQg5zFq
[4]: https://github.com/coreemu/core/releases/download/release-7.5.1/core_distributed_7.5.1_amd64.deb
