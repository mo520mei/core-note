# core-note

#### Description
This repository is mainly used to store some of my note from reading the CORE (Common Open Research Emulator) code

#### Instructions
All the files are Markdown(.md) file，you can read them with [vscode][1], [vnote][2] et.al.


[1]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiAxL_pmf7yAhVOgp4KHX7UBj8QFnoECA4QAw&url=https%3A%2F%2Fcode.visualstudio.com%2F&usg=AOvVaw15O90sm1ios8AUpw56hCml
[2]: https://github.com/vnotex/vnote


