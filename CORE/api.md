# api
[toc]


## grpc

### client.py

#### start_session
通过编译出来的*_pb2.py文件，调用[server.py](#server.py)中的[`StartSession`方法](#startsession方法)

### server.py

#### StartSession方法
**仿真开始的入口找到了！！！！**

1. 获取session：通过CoreEmu对象中保存的Int数字<-->Session对象的字典Dict来获取通过数字获取Session.
2. 运行`session.clear`清理sesion可能的遗留内容，并将session设置到初始状态。
3. 设置session状态为`CONFIGURATION_STATE`
4. 设定位置location，参考坐标的修改详见[GUI-py](./GUI-py.md)
5. 添加hooks？   不懂
6. 创建节点，调用[`grpcutils.create_nodes()`](#grpcutils.create_nodes())。需要注意的是这里创建节点的功能相当于把一系列创建节点所需要的代码都扔到了进程池中，这些代码会根据session的状态进行执行。
```py
_, exceptions = grpcutils.create_nodes(session, request.nodes)
if exceptions:
    exceptions = [str(x) for x in exceptions]
    return core_pb2.StartSessionResponse(result=False, exceptions=exceptions)
```
7. 配置EMANE
```py
#获取emane得配置信息，从/etc/core/core.conf中
config = session.emane.get_configs()
#更新request参数中的emane_config
config.update(request.emane_config)
#进行配置
for config in request.emane_model_configs:
    _id = utils.iface_config_id(config.node_id, config.iface_id)
    session.emane.set_model_config(_id, config.model, config.config)
```
8. 无线配置
```py
for config in request.wlan_configs:
    session.mobility.set_model_config(config.node_id, BasicRangeModel.name, config.config)
```
9. 移动性配置
```py
for config in request.mobility_configs:
    session.mobility.set_model_config(
        config.node_id, Ns2ScriptedMobility.name, config.config
    )
```
10. 服务（调用[grpcutils.service_configuration](#grpcutils.service_configuration())）和配置服务配置（service and config service）

```py
for config in request.service_configs:
    grpcutils.service_configuration(session, config)

for config in request.config_servie_configs:
    node = self.get_node(session, config.node_id, context, CoreNode)
    service = node.config_services[config.name]
    if config.config:
        service.set_config(config.config)
    for name, template in config.templates.items()
        service.set_template(name, template)
```
11. 服务文件配置(调用[session.services.set_service_file](services.md#coreservices.set_service_file()))从GUI中接收文件信息，在服务配置中存储自定义文件。
```py
for config in request.service_file_configs:
    session.services.set_service_file(
        config.node_id, config.service, config.file, config.data
    )
```
12. [创建连接](#grpcutils.create_links())
```py
_, exceptions = grpcutils.create_links(session, request.links)
if exceptions:
    exceptions = [str(x) for x in exceptions]
    return core_pb2.StartSessionResponse(result = False, exceptions = exceptions)
```
13. 不对称连接？？？
14. 设置session状态为`EventTypes.INSTANTIATION_STATE`
```py
session.set_state(EventTypes.INSTANTIATION_STATE)
```
15. [启动服务](emulator.md#session.instantiate())。
```py
boot_exceptions = session.instantiate()
if boot_exceptions:
    exceptions = []
    for boot_exception in boot_exceptions:
        for service_exception in boot_exception.args:
            exceptions.append(str(service_exception))
    return core_pb2.StartSessionResponse(result=False, exceptions=exceptions)
return core_pb2.StartSessionResponse(result=True)
```

### grpcutils.py

#### grpcutils.create_nodes()
这个函数遍历节点信息列表，然后把一系列的[session](./emulator.md#session.py)中的[`add_node`](./emulator.md#session.add_node)函数变成命令放到[utils](./utils.md)中的[`threadpool`](./utils.md#threadpool)中自动化执行。
```py
def create_nodes(
    session: Session, node_protos: List[core_pb2.Node]
) -> Tuple[List[NodeBase], List[Exception]]:
    """
    Create nodes using a thread pool and wait for completion.

    :param session: session to create nodes in
    :param node_protos: node proto messages
    :return: results and exceptions for created nodes
    """
    funcs = []
    for node_proto in node_protos:
        _type, _id, options = add_node_data(node_proto)
        _class = session.get_node_class(_type)
        args = (_class, _id, options)
        funcs.append((session.add_node, args, {}))
    #这里start为计时，为了计算完成节点创建花了多长时间。
    start = time.monotonic()
    results, exceptions = utils.threadpool(funcs)
    total = time.monotonic() - start
    logging.debug("grpc created nodes time: %s", total)
    return results, exceptions
```

#### grpcutils.create_links()
创建连接，将创建链路的代码放入线程池中，并根据session状态执行（调用[session.add_link](./emulator.md#session.add_link)）。
```py
def create_links(
    session: Session, link_protos: List[core_pb2.Link]
) -> Tuple[List[NodeBase], List[Exception]]:
    """
    Create links using a thread pool and wait for completion.

    :param session: session to create nodes in
    :param link_protos: link proto messages
    :return: results and exceptions for created links
    """
    funcs = []
    for link_proto in link_protos:
        node1_id = link_proto.node1_id
        node2_id = link_proto.node2_id
        iface1, iface2, options, link_type = add_link_data(link_proto)
        args = (node1_id, node2_id, iface1, iface2, options, link_type)
        funcs.append((session.add_link, args, {}))
    start = time.monotonic()
    results, exceptions = utils.threadpool(funcs)
    total = time.monotonic() - start
    logging.debug("grpc created links time: %s", total)
    return results, exceptions
```


#### grpcutils.service_configuration()
为一个节点设置服务配置得方便方法。

* 调用服务（该服务对象属于某个session）中得[设置服务方法](services.md#coreservices.set_services()
)读取配置信息，然后将节点的服务信息存储在一个实例化的服务对象中。
```py
session.services.set_service(config.node_id, config.service)
```
* 调用服务（该服务对象属于某个session）中得[获取服务方法](services.md#coreservices.get_service())根据服务名字获取匹配的服务。如果找到了就返回，找不到返回特定服务
```py
service = session.services.get_service(config.node_id, config.service)
```
```py
if config.files:
    service.configs = tuple(config.files)
if config.directories:
    service.dirs = tuple(config.directories)
if config.startup:
    service.startup = tuple(config.startup)
if config.validate:
    service.validate = tuple(config.validate)
if config.shutdown:
    service.shutdown = tuple(config.shutdown)
```







## tlv

### coreserver.py
初始化一个CORE Server：
* 实例化[CoreEmu](./emulator.md#coreemu.py)
* 读取配置
* 初始化TCP服务。
























