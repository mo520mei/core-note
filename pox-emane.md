# 基于POX控制器的无线SDN自动流表下发

## 20220429
**进度**：
- 可以通过POX控制器下发流表，实现三个交换机互相Ping通，或者n2作为中继，为n1和n3提供转发功能。
- 黄-[拓扑探测和路由计算、ARP下发](https://gitee.com/hysTOP/war_net_tran_plus)

三个节点的流表：

    root@n1:/tmp/pycore.1/n1.conf# ovs-ofctl dump-flows ovsbr1
    cookie=0x0, duration=1208.481s, table=0, n_packets=8, n_bytes=784, priority=10,ip,nw_dst=10.0.0.3 actions=mod_dl_dst:5e:4e:f9:ee:d5:4c,output:eth0
    cookie=0x0, duration=1517.643s, table=0, n_packets=8, n_bytes=784, priority=1,ip,in_port=eth0,nw_dst=10.0.0.1 actions=LOCAL
    cookie=0x0, duration=1517.636s, table=0, n_packets=3, n_bytes=266, priority=1,in_port=LOCAL actions=output:eth0

    root@n2:/tmp/pycore.1/n2.conf# ovs-ofctl dump-flows ovsbr2
    cookie=0x0, duration=31.329s, table=0, n_packets=0, n_bytes=0, priority=10,ip,nw_dst=10.0.0.3 actions=mod_dl_src:5e:4e:f9:ee:d5:4c,mod_dl_dst:4e:4a:69:22:06:42,IN_PORT
    cookie=0x0, duration=26.167s, table=0, n_packets=0, n_bytes=0, priority=10,ip,nw_dst=10.0.0.1 actions=mod_dl_src:5e:4e:f9:ee:d5:4c,mod_dl_dst:42:53:c3:28:b2:4b,IN_PORT
    cookie=0x0, duration=745.964s, table=0, n_packets=1, n_bytes=70, priority=1,in_port=LOCAL actions=output:eth0
    cookie=0x0, duration=745.971s, table=0, n_packets=0, n_bytes=0, priority=1,ip,in_port=eth0,nw_dst=10.0.0.2 actions=LOCAL


    root@n3:/tmp/pycore.1/n3.conf# ovs-ofctl dump-flows ovsbr3
    cookie=0x0, duration=131.839s, table=0, n_packets=4, n_bytes=392, priority=10,ip,nw_dst=10.0.0.1 actions=mod_dl_dst:5e:4e:f9:ee:d5:4c,output:eth0
    cookie=0x0, duration=296.717s, table=0, n_packets=6, n_bytes=588, priority=1,ip,in_port=eth0,nw_dst=10.0.0.3 actions=LOCAL
    cookie=0x0, duration=295.975s, table=0, n_packets=3, n_bytes=294, priority=1,in_port=LOCAL actions=output:eth0





POX手动下发流表命令：
```py
# -*- coding:utf8 -*-

# 加载库
from pox.lib.addresses import IPAddr, EthAddr
from pox.core import core
import pox.openflow.libopenflow_01 as openflow

# POX下发流表所使用的key目前是交换机同名端口的MAC地址，转换为10进制
key1 = hex2dec(macOfSwitch1)
key2 = hex2dec(macOfSwitch2)
key3 = hex2dec(macOfSwitch3)

# 使得交换机可以就近ping通邻居的流表下发
# Switch1
msg = of.ofp_flow_mod()
msg.priority = 1                                                            # 优先级
msg.match.in_port = 1                                                       # 匹配：入端口，EMANE做ADHOC网络时，每个交换机只有一个eth0接口绑定到网桥，这里固定为1，也就是eth0
msg.match.dl_type = 0x0800                                                  # 匹配：协议类型，0x0800时IP协议，等效于在交换机出写流表时的ip。
msg.match.nw_dst = "10.0.0.1"                                               # 匹配：IP地址，要匹配IP地址必须在前面规定协议类型，否则无效。
msg.actions.append(of.ofp_action_output(port = of.OFPP_LOCAL))              # 动作：将IP目的地是自身的包转发到LOCAL（交换机同名）port
core.openflow.connections[key1].send(msg)                                   # 下发流表

# 优先级较低的流表，将交换机所在容器产生的包通过eth0 port发送出去。
msg = of.ofp_flow_mod()
msg.priority = 1
msg.match.in_port=of.OFPP_LOCAL
msg.actions.append(of.ofp_action_output(port=1))
core.openflow.connections[key1].send(msg)


# 2
msg = of.ofp_flow_mod()
msg.priority = 1
msg.match.in_port = 1
msg.match.dl_type = 0x0800
msg.match.nw_dst = "10.0.0.2"
msg.actions.append(of.ofp_action_output(port=of.OFPP_LOCAL))
core.openflow.connections[key2].send(msg)

msg = of.ofp_flow_mod()
msg.priority = 1
msg.match.in_port=of.OFPP_LOCAL
msg.actions.append(of.ofp_action_output(port=1))
core.openflow.connections[key2].send(msg)


# 3
msg = of.ofp_flow_mod()
msg.priority = 1
msg.match.in_port = 1
msg.match.dl_type = 0x0800
msg.match.nw_dst = "10.0.0.3"
msg.actions.append(of.ofp_action_output(port=of.OFPP_LOCAL))
core.openflow.connections[key3].send(msg)

msg = of.ofp_flow_mod()
msg.priority = 1
msg.match.in_port=of.OFPP_LOCAL
msg.actions.append(of.ofp_action_output(port=1))
core.openflow.connections[key3].send(msg)





# route flow

# 1 将目的地是“10.0.0.3”的包发到“10.0.0.2”这个节点
msg = of.ofp_flow_mod()
msg.priority = 10
msg.match.dl_type=0x0800
msg.match.nw_dst = "10.0.0.3"
msg.actions.append(of.ofp_action_dl_addr.set_dst("5e:4e:f9:ee:d5:4c"))
msg.actions.append(of.ofp_action_output(port=1))
core.openflow.connections[key1].send(msg)


# 2 修改来往的包的MAC地址，转发
msg = of.ofp_flow_mod()
msg.priority = 10
msg.match.dl_type=0x0800
msg.match.nw_dst = "10.0.0.3"
msg.actions.append(of.ofp_action_dl_addr.set_src("5e:4e:f9:ee:d5:4c"))
msg.actions.append(of.ofp_action_dl_addr.set_dst("4e:4a:69:22:06:42"))
msg.actions.append(of.ofp_action_output(port=of.OFPP_IN_PORT))
core.openflow.connections[key2].send(msg)

msg = of.ofp_flow_mod()
msg.priority = 10
msg.match.dl_type=0x0800
msg.match.nw_dst = "10.0.0.1"
msg.actions.append(of.ofp_action_dl_addr.set_src("5e:4e:f9:ee:d5:4c"))
msg.actions.append(of.ofp_action_dl_addr.set_dst("42:53:c3:28:b2:4b"))
msg.actions.append(of.ofp_action_output(port=of.OFPP_IN_PORT))
core.openflow.connections[key2].send(msg)




# 3 目的地是节点1的包发往节点2.
msg = of.ofp_flow_mod()
msg.priority = 10
msg.match.dl_type=0x0800
msg.match.nw_dst = "10.0.0.1"
msg.actions.append(of.ofp_action_dl_addr.set_dst("5e:4e:f9:ee:d5:4c"))
msg.actions.append(of.ofp_action_output(port=1))
core.openflow.connections[key3].send(msg)

```





