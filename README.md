# core-note

#### 介绍
这个仓库主要用来存储我阅读CORE(Common Open Research Emulator)代码中的一些经验。

#### 使用说明
文件内容均为Markdown(.md)文件，推荐在线浏览或者使用[vscode][1]、[vnote][2]或者其他支持markdown文件的文本编辑器打开。考虑到pdf难以修改，所以没有生成pdf文件。上述软件均可以根据md文件生成pdf。


[1]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiAxL_pmf7yAhVOgp4KHX7UBj8QFnoECA4QAw&url=https%3A%2F%2Fcode.visualstudio.com%2F&usg=AOvVaw15O90sm1ios8AUpw56hCml
[2]: https://github.com/vnotex/vnote